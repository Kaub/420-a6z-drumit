package ca.cs.multimedia.drumit.activities;

import android.annotation.SuppressLint;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import ca.cs.multimedia.drumit.R;
import ca.cs.multimedia.drumit.models.Bounds;
import ca.cs.multimedia.drumit.models.Percussion;
import ca.cs.multimedia.drumit.models.Point;

public class MainActivity extends AppCompatActivity {

    final int MAX_STREAM = 7;
    final int PRIORITY = 1;
    final float ORIGINAL_IMAGE_WIDTH = 1862f;
    final float ORIGINAL_IMAGE_HEIGHT = 1631f;

    Percussion[] mPercussions = new Percussion[7];

    SoundPool mSoundPool;

    ImageView mDrumImageView;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDrumImageView = findViewById( R.id.drumImageView );

        //Instancier l'objet SoundPool
        mSoundPool = new SoundPool.Builder().setMaxStreams(MAX_STREAM).build();

        mPercussions[0] = new Percussion("Kick");
        mPercussions[1] = new Percussion("Snare");
        mPercussions[2] = new Percussion("Tom1");
        mPercussions[3] = new Percussion("Tom2");
        mPercussions[4] = new Percussion("Tom3");
        mPercussions[5] = new Percussion("Hat");
        mPercussions[6] = new Percussion("Crash");

        //Associez les SoundID aux percussions

        mPercussions[0].setSoundID(mSoundPool.load(this,R.raw.kick,1));
        mPercussions[1].setSoundID(mSoundPool.load(this,R.raw.snare,1));
        mPercussions[2].setSoundID(mSoundPool.load(this,R.raw.tom1,1));
        mPercussions[3].setSoundID(mSoundPool.load(this,R.raw.tom2,1));
        mPercussions[4].setSoundID(mSoundPool.load(this,R.raw.tom3,1));
        mPercussions[5].setSoundID(mSoundPool.load(this,R.raw.hat,1));
        mPercussions[6].setSoundID(mSoundPool.load(this,R.raw.crash,1));


        mDrumImageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch(event.getActionMasked()) {

                    case MotionEvent.ACTION_DOWN:

                        Point touch = new Point(event.getX(),event.getY());

                        //Vérifiez chaque percussion à savoir si elle a été touchée
                        
                }

                return true;
            }
        });

    }


    /**
     * Fonction qui informe si le point touché est dans les bornes
     * @param bounds Les bornes du rectangle à vérifier
     * @param touch Le point ou le doigt a touché
     * @return
     */
    public boolean isTouched(Bounds bounds, Point touch) {

        //Complétez le code ici
        return false;
    }

    /**
     * Fonction qui s'occupe de jouer la percussion.
     * @param percussion Percussion touchée
     */
    public void doHitPercussion(Percussion percussion) {
        //Complétez le code ici

    }

    /**
     * Configuration des bornes de touche des percussions.
     * @param hasFocus
     */
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        mPercussions[0].setBounds(  new Bounds(new Point(200 / ORIGINAL_IMAGE_WIDTH * mDrumImageView.getWidth(), 900 / ORIGINAL_IMAGE_HEIGHT * mDrumImageView.getHeight()), new Point(800 / ORIGINAL_IMAGE_WIDTH * mDrumImageView.getWidth(), 1600 / ORIGINAL_IMAGE_HEIGHT * mDrumImageView.getHeight())) );
        mPercussions[1].setBounds(  new Bounds(new Point(1050 / ORIGINAL_IMAGE_WIDTH * mDrumImageView.getWidth(), 740 / ORIGINAL_IMAGE_HEIGHT * mDrumImageView.getHeight()), new Point(1450 / ORIGINAL_IMAGE_WIDTH * mDrumImageView.getWidth(), 1080 / ORIGINAL_IMAGE_HEIGHT * mDrumImageView.getHeight())) );
        mPercussions[2].setBounds(  new Bounds(new Point(75 / ORIGINAL_IMAGE_WIDTH * mDrumImageView.getWidth(), 600 / ORIGINAL_IMAGE_HEIGHT * mDrumImageView.getHeight()), new Point(380 / ORIGINAL_IMAGE_WIDTH * mDrumImageView.getWidth(), 880 / ORIGINAL_IMAGE_HEIGHT * mDrumImageView.getHeight())) );
        mPercussions[3].setBounds(  new Bounds(new Point(320 / ORIGINAL_IMAGE_WIDTH * mDrumImageView.getWidth(), 420 / ORIGINAL_IMAGE_HEIGHT * mDrumImageView.getHeight()), new Point(660 / ORIGINAL_IMAGE_WIDTH * mDrumImageView.getWidth(), 780 / ORIGINAL_IMAGE_HEIGHT * mDrumImageView.getHeight())) );
        mPercussions[4].setBounds(  new Bounds(new Point(740 / ORIGINAL_IMAGE_WIDTH * mDrumImageView.getWidth(), 440 / ORIGINAL_IMAGE_HEIGHT * mDrumImageView.getHeight()), new Point(1110 / ORIGINAL_IMAGE_WIDTH * mDrumImageView.getWidth(), 890 / ORIGINAL_IMAGE_HEIGHT * mDrumImageView.getHeight())) );
        mPercussions[5].setBounds(  new Bounds(new Point(1200 / ORIGINAL_IMAGE_WIDTH * mDrumImageView.getWidth(), 390 / ORIGINAL_IMAGE_HEIGHT * mDrumImageView.getHeight()), new Point(1850 / ORIGINAL_IMAGE_WIDTH * mDrumImageView.getWidth(), 610 / ORIGINAL_IMAGE_HEIGHT * mDrumImageView.getHeight())) );
        mPercussions[6].setBounds(  new Bounds(new Point(0 / ORIGINAL_IMAGE_WIDTH * mDrumImageView.getWidth(), 0 / ORIGINAL_IMAGE_HEIGHT * mDrumImageView.getHeight()), new Point(640 / ORIGINAL_IMAGE_WIDTH * mDrumImageView.getWidth(), 390 / ORIGINAL_IMAGE_HEIGHT * mDrumImageView.getHeight())) );
    }
}
